# Borgbakcup container

A container with **borgbackup** and **openssh** to host backup repositories.

## Initialization

Add a file `authorized_keys` at root of the repo that contains your public ssh keys and restrictions. You can follow `authorized_keys.example` and [borg documentation](https://borgbackup.readthedocs.io/en/stable/usage/serve.html).

Add the path to your repositories directory in a `.env` file (following `.env.example`).