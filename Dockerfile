FROM alpine

RUN apk add --no-cache borgbackup openssh-server && \
  mkdir /repositories

COPY entrypoint.sh .

VOLUME /root/.ssh/
VOLUME /etc/ssh/
VOLUME /repositories/

EXPOSE 22

ENTRYPOINT ["./entrypoint.sh"]